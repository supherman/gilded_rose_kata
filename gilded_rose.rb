def update_quality(items)
  items.each do |item|
    RuleFactory.build(item).apply_rules
  end
end

module RuleFactory
  def self.build(item)
    strategies[item.name].new(item)
  end

  def self.strategies
    strategies                                              = Hash.new(Normal)
    strategies['Aged Brie']                                 = Aged
    strategies['Sulfuras, Hand of Ragnaros']                = Sulfuras
    strategies['Backstage passes to a TAFKAL80ETC concert'] = Backstage
    strategies['Conjured Mana Cake']                        = Conjured
    strategies
  end
end

class Normal
  def initialize(item)
    @item = item
  end

  def apply_rules
    @item.sell_in -= 1
    if @item.sell_in > 0
      @item.quality -= 1
    else
      @item.quality -= 2
    end if @item.quality > 0
  end
end

class Aged
  def initialize(item)
    @item = item
  end

  def apply_rules
    @item.sell_in -= 1
    if @item.sell_in > 0
      @item.quality += 1
    else
      @item.quality += 2
    end if @item.quality < 50
  end
end


class Sulfuras
  def initialize(item)
    @item = item
  end

  def apply_rules; end
end

class Backstage
  INF = 1.0/0

  def initialize(item)
    @item = item
  end

  def apply_rules
    @item.sell_in -= 1
    if @item.quality < 50
      case @item.sell_in
        when 5..9
          @item.quality += 2
        when 0..4
          @item.quality += 3
        when -INF..0
          @item.quality = 0
        else
          @item.quality += 1
      end
    end
  end
end

class Conjured
  def initialize(item)
    @item = item
  end

  def apply_rules
    @item.sell_in -= 1
    if @item.sell_in > 0
      @item.quality -= 2
    else
      @item.quality -= 4
    end if @item.quality > 0
  end
end

# DO NOT CHANGE THINGS BELOW -----------------------------------------

Item = Struct.new(:name, :sell_in, :quality)

# We use the setup in the spec rather than the following for testing.
#
# Items = [
#   Item.new("+5 Dexterity Vest", 10, 20),
#   Item.new("Aged Brie", 2, 0),
#   Item.new("Elixir of the Mongoose", 5, 7),
#   Item.new("Sulfuras, Hand of Ragnaros", 0, 80),
#   Item.new("Backstage passes to a TAFKAL80ETC concert", 15, 20),
#   Item.new("Conjured Mana Cake", 3, 6),
# ]

